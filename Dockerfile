FROM python:latest
WORKDIR /app
ADD . /app/
RUN pip install --upgrade pip
RUN pip install -r requierments.txt
CMD gunicorn app:app --bind 0.0.0.0:$PORT --reload